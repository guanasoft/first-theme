<?php get_header()?>
<?php get_sidebar()?>
<div id="left">
    
   <?php while(have_posts()): the_post()?>
    
    <h2><?php the_title()?></h2>
    <?php 

    if ( is_user_logged_in() ) {
      the_content();
} else {
    echo 'Welcome, visitor!';
}

    
   
    
    
    
    ?>
    
    <?php endwhile;?>
    
</div>


<?php get_footer()?>
