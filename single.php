<?php get_header()?>
<?php get_sidebar()?>
<div id="left">
    
    <?php while(have_posts()): the_post();?>
    
    <h2><?php the_title()?></h2>
    <?php 
    if ( is_user_logged_in() ) {
         $postid = get_the_ID();
         $name = get_post_meta( $postid, 'theme_username_customField', true );
         echo 'Welcome, '. $name ;
        the_content();
} else {
    echo 'Welcome, visitor!';
}

    
   ?>
    
    <?php endwhile;?>
    
    <?php comments_template('',true);?>
    
</div>

<?php get_footer()?>
