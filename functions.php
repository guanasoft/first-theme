<?php

register_sidebar(array(
    'name' => __('1st Right Sidebar'),
    'id' => 'first-right-sidebar',
    'description' => 'The top Bar',
    'before_widget' => '<div>',
    'after_widget' => '</div>'
    
    ));

register_sidebar(array(
    'name' => __('2nd Right Sidebar'),
    'id' => 'second-right-sidebar',
    'description' => 'The second top Bar',
    'before_widget' => '<div>',
    'after_widget' => '</div>'
    ));


function alertVisitor(){
    echo '<script type="text/javascript"> jQuery( document ).ready(function() {alert("Hola mundo!") ; }); </script>';
}
add_action('wp_head', 'alertVisitor');


//adding example metabox
function jdev_add_custom_box( $post ) {
 
        add_meta_box(
                'Meta Box', // ID, should be a string
                'Custom Meta Box', // Meta Box Title
                'jdev_custom_meta_box_content', // Your call back function, this is where your form field will go
                'post', // The post type you want this to show up on, can be post, page, or custom post type
                'normal', // The placement of your meta box, can be normal or side
                'high' // The priority in which this will be displayed
            );
 
}
add_action( 'add_meta_boxes', 'jdev_add_custom_box' );

function jdev_custom_meta_box_content( $post ) {
        echo '<label>Your Name</label><input id="username" name="username" value="'.get_post_meta( $post->ID, 'theme_username_customField', true ) .'" />';
}


function saveUsername($post_id){
    $username = $_POST['username'];
    update_post_meta( $post_id, 'theme_username_customField', sanitize_text_field( $username ) );
}
add_action('save_post','saveUsername');



?>